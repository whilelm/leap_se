@title = 'Bitmask for Mac'
@author = 'kali'
@posted_at = '2017-10-05'
@more = true
@preview_image = '/img/pages/bitmask.png'

After a massive refactor, we have now are happy to release Bitmask for Mac! [Try the beta](https://bitmask.net/). 

Known issues with state changes and false errors on start up are being fixed. 

Please help us making Bitmask better, by testing it and filing any bug reports in [the new issue tracker](https://0xacab.org/leap/bitmask-dev/issues)

The Bitmask team.
