@title = 'Bitmask 0.10 released, massive refactor'
@author = 'kali'
@posted_at = '2017-10-04'
@more = true
@preview_image = '/img/pages/bitmask.png'

We are pleased to announce the Bitmask 0.10 release, codename "la rosa de foc". You can refer to the [changelog](https://0xacab.org/leap/bitmask-dev/blob/0.10rc2/docs/changelog.rst) for all the changes.

A fresh UI, and increased stability for the VPN are only the tip of the iceberg. This long awaited refactor has countless backend improvements for both Bitmask VPN and Bitmask Mail that put us on course for much more frequent releases. [Try it while it's still fresh](https://bitmask.net/)

This is still a beta-only release of the Encrypted Email service, that ships again with the Pixelated email user agent. We have also ported the VPN service from the legacy 0.9.2 bitmask client. This is the first release of the new codebase that ships VPN, so expect some rough behavior.

The demo provider for the Mail service you should use with this bundle is https://mail.bitmask.net. For VPN, you can keep using https://demo.bitmask.net

Please help us making Bitmask better, by testing it and filing any bug reports in [the new issue tracker](https://0xacab.org/leap/bitmask-dev/issues)

Until the next release, see you on the intertubes, and stay safe.

The Bitmask team.
