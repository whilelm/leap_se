@title = 'tor'
@summary = 'Tor services: relay, exit node and hidden service'

Topology
------------------------

Nodes with `tor` service will run a Tor relay with some pre-defined settings, which can be changed with some configuration (see *Configuration* below). You can enable an exit or a hidden service with additional configuration.

Configuration
------------------------------

By default, if a node has service 'tor' configured, it will run a tor relay (not an exit). The relay will be configured with bandwidth limitations, contacts, a nickname and a family. The defaults for these (shown below), can be overridden as desired.

* `tor.bandwidth_rate`: the max bandwidth allocated to Tor, in KB per second, when used as an exit node (default: 6550 KB/sec).
* `tor.type`: what type of tor node to make, at this moment only 'exit' is supported. If not specified, acts as a relay.
* `tor.contacts`: the contact information for the relay (default: the list of provider contacts)
* `tor.nickname`: the nickname of the relay (default: a combination of the node name and a hash of the family)
* `tor.family`: a list of the other nicknames that are part of the same provider
* `tor.hidden_service`: to enable a hidden service, set 'active' to be true (see below for an example), do *not* configure "services": ["tor"] for the node!

Examples:

To add a relay to a node:

    {
     "services": ["tor"]
    }

To enable a hidden service, without a relay, do *not* specify the tor service (it is not considered secure to have a node configured as a relay and a hidden service at the same time, see: https://trac.torproject.org/8742), instead configure the node to have the following: 

    {
      "tor": {
        "hidden_service": {
        "active": true
      }
    }

If activated, you can list the hidden service .onion addresses this way:

   leap ls --print tor.hidden_service.address tor

Then just add '.onion' to the end of the printed addresses.

To enable a Tor exit node:

    {
      "tor": {
        "bandwidth_rate": 6550,
        "type": "exit"
      }
    }


